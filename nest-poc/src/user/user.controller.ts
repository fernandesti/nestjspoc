import { Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, UsePipes, ValidationPipe } from "@nestjs/common";
import { NestResponseBuilder } from "src/core/NestResponseBuilder";
import { User } from "./user.model";
import { UserService } from "./user.service";

@Controller('user')
export class UserController{

    constructor(private userService: UserService){}

    @Post()
    create(@Body() user: User){
        const usuarioCriado = this.userService.create(user);
        return new NestResponseBuilder()
                   .setStatus(HttpStatus.CREATED)
                   .setHeaders({
                       'location': `/users/${usuarioCriado.name}`
                   })
                   .setBody(usuarioCriado)
                   .build();
    }

    @Get()
    users(){
        return this.userService.showUsers();
    }

    @Get(':name')
    user(@Param('name') name: string){
        const user = this.userService.user(name);
        if(!user){
            throw new NotFoundException({
                statusCode: HttpStatus.NOT_FOUND,
                message: 'User not found'
            });
        }
        return user;
    }
}