import { Module } from "@nestjs/common";
import { UserController } from "./user.controller";
import { UserService } from "./user.service";
import { IsUniqueUserConstraint } from "../pipe/is-unique-user";

@Module({
    imports: [],
    controllers: [ UserController ],
    providers: [ UserService, IsUniqueUserConstraint ],
  })
export class UserModule{}