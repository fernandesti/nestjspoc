import { Exclude } from "class-transformer";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { IsUniqueUser } from "src/pipe/is-unique-user";

export class User {

    @IsNotEmpty()
    id: number;

    @IsString()
    @IsNotEmpty()
    @IsUniqueUser(
        "The user already exits!"
    , {})
    name: string;

    @IsNotEmpty()
    @Exclude({
        toPlainOnly: true
    })
    password: string;

    @IsEmail({}, {
        message: "Field is not an email!"
    })
    @IsNotEmpty()
    email: string;

    date: Date;

}