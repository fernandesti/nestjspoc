import { Injectable } from "@nestjs/common";
import { User } from "./user.model";

@Injectable()
export class UserService{

    private users: User[] = []

    create(user: User): User{
        user.date = new Date();
        this.users.push(user);
        return user;
    }

    async showUsers() :Promise<User[]> {
        return this.users;
    }

     user(name: string) :User{
        return this.users.find(u => u.name == name);
    }

}