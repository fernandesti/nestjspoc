import { NestResponse } from "./NestResponse"

export class NestResponseBuilder {
    private response: NestResponse = {
        status: 200,
        headers: {},
        body: {}
    }

    public setStatus(status: number){
        this.response.status = status;
        return this;
    }

    public setHeaders(header: object){
        this.response.headers = header;
        return this;
    }

    public setBody(body: object){
        this.response.body = body;
        return this;
    }

    public build(){
        return new NestResponse(this.response);
    }
};

