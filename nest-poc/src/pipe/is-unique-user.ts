import { Injectable } from '@nestjs/common';
import { registerDecorator, ValidationOptions, ValidationArguments, ValidatorConstraintInterface, ValidatorConstraint } from 'class-validator';
import { UserService } from 'src/user/user.service';

@Injectable()
@ValidatorConstraint()
export class IsUniqueUserConstraint implements ValidatorConstraintInterface {
    constructor(private userService: UserService){}
    
    validate(userName: string, validationArguments?: ValidationArguments): boolean | Promise<boolean> {
        return !!!this.userService.user(userName);
    } 
}

export function IsUniqueUser(property: string, validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      //name: 'IsUniqueUser',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: IsUniqueUserConstraint
    });
  };
}