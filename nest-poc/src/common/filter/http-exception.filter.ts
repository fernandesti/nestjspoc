import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from "@nestjs/common";
import { AbstractHttpAdapter, HttpAdapterHost } from "@nestjs/core";

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    
    private adapterHost: AbstractHttpAdapter;

    constructor(_adapterHost: HttpAdapterHost) {
        this.adapterHost = _adapterHost.httpAdapter;
    }
    
    catch(exception: Error, host: ArgumentsHost) {
         const context = host.switchToHttp();
         const request = context.getRequest();
         const response = context.getResponse();

         const { status, body } = exception instanceof HttpException
         ? 
         {
            status: exception.getStatus(),
            body: exception.getResponse()
         }
         : 
         {
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            body: {
                status: HttpStatus.INTERNAL_SERVER_ERROR,
                timestamp: new Date().toISOString(),
                message: exception.message,
                path: request.path
            }
         }

         this.adapterHost.reply(response, body, status);
    }
}